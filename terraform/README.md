
[comment]: # (Docs file for Terraform module: gitlab-ee)

| Name | Type | Default/Required | Description |
|---|---|---|---|
| env | string | required | abbreviated name of environment that the resource will be deployed to. eg: gc, gt, gd |
| stage | string | Dev | Current development stage. eg: Dev, Staging, Prod |
| env_vpc | string | required | abbreviated name of VPC that the resource will be deployed to. eg: gc, gt, gd |
| instance_type | string | t3.small | aws instance type |
| subnet_tags | map | { Role : "mgmt" } | Map of tags/values used to identify the subnet that your resource will deploy into. |
| app_name | string | gitlab | Name of the application that you will be deploying |
| gl_db_username | string | svc_gl_db |  |
| mm_db_username | string | svc_mm_db |  |
| rest_version | string | 1.2 |  |
| s3_prefix | string | gitlab |  |
| asg_max_size | string | 1 | Maximum number of instances for your autoscaling group |
| asg_min_size | string | 1 | Minimum number of instances for your autoscaling group |
| asg_desired | string | 1 | desired number of instances for your autoscaling group |
| release | string | required | release version number of the application you will be deploying |
| img_owners | list | [ "self" ] | List of AWS account IDs used to search for AMIs |
| extra_sgs | list | [  ] | List of additional security group ids to add to your instance |
| user_data | string |   | base64-encoded string that can be used to configure your instance |
| instance_auto_on | string | no | Determines whether or not your instance will be turned on every morning at 6:00 AM. |
| instance_auto_off | string | yes | Determines whether or not your instance will be turned off every evening at 7:00 PM. |
| s3_bucket | string | gd-adte-code | s3 bucket for storing deployment server data |
| s3_prefix | string | systems/gitlab | s3 prefix for storing deployment server data |
