terraform {
  required_version = "~> 0.12"

  backend "s3" {
    bucket         = "gd-adte-tf-state"
    key            = "gd/vdms/systems/gitlab-geo.tfstate"
    region         = "us-gov-west-1"
    profile        = "il5-dev"
    dynamodb_table = "terraform"
  }
}
