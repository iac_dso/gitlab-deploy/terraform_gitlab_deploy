In conjunction, the files below create a systemd daemon for continuous backup
of important files and directories immediately upon change.  The first file is
called `path unit` and is responsible watching for changes in files and directories.
Once a change is detected, the path unit activates its corresponding
`service unit`.




```ini
# Example Path unit
[Unit]
Description = "Monitors files in '{{ local backup directory }}' for changes. When a change is detected {{ unit name }}.service is activated."

[Path]
PathChanged={{ local backup directory }}
Unit= {{ unit name }}.service

[Install]
WantedBy=multi-user.target
```

The systemd `service unit` handles the execution of the backup by calling the
`aws s3 sync` command.

```ini
# Example service unit
[Unit]
Description = "Syncs {{ backup item }} to S3 on write. Used for backup & restore."

[Service]
Type=simple
Environment="{{ aws region }}"
ExecStart= sudo /usr/bin/aws s3 sync {{ local backup directory }} s3://{{ s3 bucket }}/{{ s3 prefix }}
```

Use the commands below to enable continuous backups.

```bash
systemctl enable {{ unit name }}.path # Enable automatic start-up on boot.
systemctl start {{ unit name }}.path # Start continuous backup service
```
