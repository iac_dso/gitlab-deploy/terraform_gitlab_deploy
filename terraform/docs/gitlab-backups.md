Base path:
`s3://gc-adte-apps/gitlab/backups/{{ version }}`, where _**`version`**_ refers to the current adte-gitlab application version.

|Local Dir | S3 location | Description
|:--- |:---:|:---|
| `/etc/ssh`|`.../gl-ssh/` |SSH host keys & authorized hosts|
| `/etc/gitlab`|`.../gl-gl/` |GL configs, certs, secrets|
| `/var/opt/gitlab/git-data/`|`.../gl-git/` |GL groups & Git repositories|

```bash
# Contains Gitlab configurations & certificats
/etc/gitlab
├── gitlab.rb
├── gitlab-secrets.json
├── ssl
│   ├── code.adtegc.dev.crt
│   └── code.adtegc.dev.key
└── trusted-certs
    ├── code.adtegc.dev.crt
    ├── code.adtegc.dev.pb7
    └── gc-sca-02b-vdms.cer
```

```bash
# Mattermost Config directory
/var/opt/gitlab/mattermost/
├── client-plugins
├── config.json # This file is a good reference for mattermost config options
├── data
├── plugins
└── VERSION
```

```bash
# Currently installed mattermost plugins.
/var/opt/gitlab/mattermost/plugins
├── com.github.manland.mattermost-plugin-gitlab
├── com.mattermost.aws-sns
├── com.mattermost.custom-attributes
├── com.mattermost.nps
├── com.mattermost.welcomebot
├── github
├── jira
├── mattermost-autolink
└── zoom
```

```bash
# The directories here contain contain Gitlab groups.
/var/opt/gitlab/git-data/repositories/
├── archived-projects
├── asparks
├── dc-i-adte
├── dfigueroa
├── jhoward
├── misc_tools
└── tf-tools
```

```bash
# The actual Git repositories are stored within in the group directories
/var/opt/gitlab/git-data/repositories/dc-i-adte/
├── ansible.git
├── ansible.wiki.git
├── cloudformation.git
├── cloudformation.wiki.git
├── eas.git
├── eas.wiki.git
├── research-engineering.git
└── research-engineering.wiki.git
```
