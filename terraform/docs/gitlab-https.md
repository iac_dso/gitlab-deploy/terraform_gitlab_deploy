<u>**Gitlab HTTPS configuration:**<br></u>

```ruby
external_url 'https://code.adtegc.dev'

nginx['listen_https'] = true # Enable HTTPS
nginx['listen_port'] = 443
letsencrypt['enable'] = false # Disable let's encrypt cert signing
nginx['redirect_http_to_https'] = true # Enable HTTP redirection
nginx['redirect_http_to_https_port'] = 80
# Point Nginx to Gitlab SSL keys
nginx['ssl_certificate'] = "/etc/gitlab/ssl/code.adtegc.dev.crt"
nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/code.adtegc.dev.key"
```
 
