<u>**Create SSL key & signed Certificate for Gitlab:**<br></u>
  - **Create RSA Key:**<br>

    ```bash
    # Note: The gitlab server expects both the key and certificate to
    # be named after after its external URL.
    openssl genrsa -out code.adtegc.dev.key 2048
    ```
  - **Create an OpenSSL configuration file for the certificate signing request(CSR) :**<br>
    ```ini
    # // From openssl.crt
    # This file sets the contains the settings required to create
    # a certificate signing request for the ADTE gitlab server.

    [req]
    distinguished_name = req_distinguished_name
    req_extensions = v3_req
    prompt=no

    [req_distinguished_name]
    countryName = US
    stateOrProvinceName = VA
    localityName = Stafford
    organizationalUnitName = adtegc
    commonName = code.adtegc.dev

    [ v3_req ]
    # Extensions to add to a certificate request
    basicConstraints = CA:FALSE
    keyUsage = nonRepudiation, digitalSignature, keyEncipherment
    subjectAltName = @alt_names

    [alt_names]
    # FQDN's for the other services on the Gitlab server
    DNS.1 = code.adtegc.dev        # Gitlab main
    DNS.2 = chatops.adtegc.dev     # MatterMost
    DNS.3 = containers.adtegc.dev  # Container registry
    ```

- **Generate CSR:**<br>
  ```bash
  # Use the "openssl req" command to generate a new CSR using the key & config
  # file created earlier
  openssl req -new \
    -out code.adtegc.dev.csr \
    -key code.adtegc.dev.key \
    -config openssl.cnf

  ```

- **Submit CSR to certificate authority for signing:**<br>
  - Navigate to any of the following URLs and when prompted sign in using your adte credentials.
    -  https://gc-sca-02b-vdms.adtegc.dev/CertSrv
    -  https://gc-sca-02b-vdms.adtegc.dev/CertSrv
  -  Under **_"Select a task"_** click on **_"Request a certificate"._** Then, once the next page is finished loading click on **_"advanced certificate request"_**.
  - On page: **"Submit a Certificate Request or Renewal Request"**
    -  Paste the contents of the CSR into the "saved request" field.
    -  In the **_"Certificate Template"_** drop-down menu select **_"adtegc web server"_**
    -  Select submit to move on to the next page
  - On page: **"Certificate Issued"**
    - Select the radial button marked **"Base 64 encoded"**
    - Click on the **"Download certificate"** link.
- **Upload the key and signed certificate to the Gitlab server:**<br>
  - Before uploading, rename the certificate to: `code.adtegc.dev.crt`
  - Place both key and certificate in the `/etc/gitlab/ssl/` directory on the Gitlab server.
  
