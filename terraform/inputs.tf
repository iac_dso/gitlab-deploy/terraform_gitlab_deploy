variable "env" {
  description = "abbreviated name of environment that the resource will be deployed to. eg: gc, gt, gd"
  type        = string
  default     = "gd"
}

variable "stage" {
  description = "Current development stage. eg: Dev, Staging, Prod"
  type        = string
  default     = "dev"
}

variable "env_vpc" {
  description = "abbreviated name of VPC that the resource will be deployed to. eg: gc, gt, gd"
  type        = string
  default     = "vdms"
}

variable "instance_type" {
  description = "aws instance type"
  type        = string
  default     = "t3.small"
}

variable "subnet_tags" {
  description = "Map of tags/values used to identify the subnet that your resource will deploy into."
  type        = map
  default     = { Role : "mgmt" }
}

variable "app_name" {
  description = "Name of the application that you will be deploying"
  type        = string
  default     = "gitlabgeo"
}

variable "gl_db_username" {
  description = ""
  type        = string
  default     = "svc_gl_db"
}

variable "mm_db_username" {
  description = ""
  type        = string
  default     = "svc_mm_db"
}

variable "rest_version" {
  description = ""
  type        = string
  default     = 1.2
}

variable "asg_max_size" {
  description = "Maximum number of instances for your autoscaling group"
  type        = string
  default     = 1
}

variable "asg_min_size" {
  description = "Minimum number of instances for your autoscaling group"
  type        = string
  default     = 1
}

variable "asg_desired" {
  description = "desired number of instances for your autoscaling group"
  type        = string
  default     = 1
}

variable "release" {
  description = "release version number of the application you will be deploying"
  type        = string
}

variable "img_owners" {
  description = "List of AWS account IDs used to search for AMIs"
  type        = list
  default     = ["self"]
}

variable "extra_sgs" {
  description = "List of additional security group ids to add to your instance"
  type        = list
  default     = []
}

variable "user_data" {
  description = "base64-encoded string that can be used to configure your instance"
  type        = string
  default     = ""
}

variable "instance_auto_on" {
  description = "Determines whether or not your instance will be turned on every morning at 6:00 AM."
  type        = string
  default     = "no"
}

variable "instance_auto_off" {
  description = "Determines whether or not your instance will be turned off every evening at 7:00 PM."
  type        = string
  default     = "yes"
}

variable "s3_bucket" {
  description = "s3 bucket for storing deployment server data"
  type        = string
  default     = "gd-adte-code"
}

variable "s3_prefix" {
  description = "s3 prefix for storing deployment server data"
  type        = string
  default     = "systems/gitlab"
}

variable "rds_pg_username" {
  description = "Postgres user name"
  default     = "svc_rds_postgres"
}

variable "rds_pg_pwd" {
  description = "Postgres user password"
}
