# Main Configuration file for Terraform module: gitlab-ee


module "vdms_rds_postgres" {
  source          = "git::ssh://git@code.adtegc.dev/dc-i-adte/common/terraform-modules/systems/rds-postgres.git"
  vpc             = data.aws_vpc.vpc
  env             = var.env
  app_name        = var.app_name
  env_vpc         = var.env_vpc
  rds_pg_pwd      = data.aws_ssm_parameter.gl_db_pwd.value
  rds_pg_username = var.gl_db_username
}

data "template_file" "gitlab_user_data" {
  template = file("${path.module}/gitlab-user-data.yml")
  vars = {
    hostname       = local.hostname
    env            = var.env
    db_database    = module.vdms_rds_postgres.db_data[0].name
    gl_db_password = data.aws_ssm_parameter.gl_db_pwd.value
    gl_db_username = var.gl_db_username
    mm_db_password = data.aws_ssm_parameter.mm_db_pwd.value
    mm_db_username = var.mm_db_username
    db_endpoint    = module.vdms_rds_postgres.db_data[0].address
    s3_bucket      = var.s3_bucket
    s3_prefix      = var.s3_prefix
    version        = var.release
    rest_version   = var.rest_version
    mm_api_id      = data.aws_ssm_parameter.mm_api_id.value
    mm_api_secret  = data.aws_ssm_parameter.mm_api_id.value
    bind_pw        = data.aws_ssm_parameter.bind_pw.value
  }
  depends_on = [module.vdms_rds_postgres]
}

module "gitlab_asg_lt" {
  source            = "git::ssh://git@code.adtegc.dev/dc-i-adte/common/terraform-modules/systems/ec2-asg-lt.git"
  pol_data          = local.pol_data
  stage             = var.stage
  env               = var.env
  env_vpc           = var.env_vpc
  subnet_tags       = var.subnet_tags
  ami_tags          = local.ami_tags
  sg_rules_in       = local.sg_rules_in
  app_name          = var.app_name
  instance_type     = local.instance_type
  user_data         = data.template_file.gitlab_user_data.rendered
  release           = var.release
  asg_min_size      = var.asg_min_size
  asg_max_size      = var.asg_max_size
  asg_desired       = var.asg_desired
  instance_auto_off = var.instance_auto_off
  instance_auto_on  = var.instance_auto_on
}
