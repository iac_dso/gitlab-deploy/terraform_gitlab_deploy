# Variable file for Terraform module: gitlab-ee
# This file should contain any local or input variables.

// input vars

// Gather environment information
data "aws_vpc" "vpc" {
  tags = {
    Name : "${var.env}-${var.env_vpc}-vpc"
  }
}

// subnets
locals {
  hostname      = "${var.env}-${var.app_name}-${var.release}-${var.env_vpc}"
  instance_type = "t3.large"
  sg_rules_in = [
    [
      "Allow HTTPS inbound",
      443,
      443,
      "tcp",
      "10.96.0.0/11,10.128.0.0/14"
    ],
    [
      "Allow SSH inbound",
      22,
      22,
      "tcp",
      "10.96.0.0/11,10.128.0.0/14"
    ],
    [
      "Allow http inbound",
      80,
      80,
      "tcp",
      "10.96.0.0/11,10.128.0.0/14"
    ]
  ]
  sg_rules_out = [

  ]
}

locals {
  pol_data = <<EOF
{
  "Statement": [
      {
          "Action": [
              "ssm:*",
              "s3:*",
              "ec2:*",
              "elasticfilesystem:*",
              "kms:DescribeKey",
              "kms:ListAliases"
          ],
          "Effect": "Allow",
          "Resource": "*",
          "Sid": ""
      }
  ],
  "Version": "2012-10-17"
}
EOF
}

locals {
  ami_tags = {
    Application = "gitlab"
    Release     = var.release
  }
}

data "aws_ssm_parameter" "gl_db_pwd" {
  name = "/gitlab/db_password"
}
data "aws_ssm_parameter" "mm_db_pwd" {
  name = "/gitlab/mm_db_password"
}
data "aws_ssm_parameter" "bind_pw" {
  name = "/gitlab/bind_password"
}
data "aws_ssm_parameter" "mm_api_id" {
  name = "/gitlab/mm_api_id"
}
data "aws_ssm_parameter" "mm_api_secret" {
  name = "/gitlab/mm_api_secret"
}
